#!/bin/bash - 
#===============================================================================
#
#          FILE: .release.sh
# 
#         USAGE: ./.release.sh <VERSION>
# 
#   DESCRIPTION: This Script is only to automate the Release Process of the
#                changelog-Script.
# 
#       OPTIONS: <VERSION>
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Francesco Emanuel Bennici <benniciemanuel78@gmail.com>
#  ORGANIZATION: 
#       CREATED: 24.03.2019 14:42:37
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

if [[ -z "${1}" ]]; then
  echo "Version Argument is missing!"
  echo "Usage: ./.release.sh <VERSION>"

  exit -1
fi

rm -f .changelog.bak .version.bak
cp changelog .changelog.bak
cp .version .version.bak

curr_ver=$(cat .version)
sed -i "s/VERSION=\"${curr_ver}\"/VERSION=\"${1}\"/g" changelog
echo -n "${1}" > .version
